"""
ID: rendyar1
LANG: PYTHON3
TASK: barn1
"""
with open('barn1.in', 'r') as f:
    lines = f.readlines()
    boards, totalstalls, cows = list(map(int, lines[0].replace("\n", '').split(" ")))
    covered = cows
    stalls = list(map(int, lines[1:]))
    stalls.sort()

gaps = []

class Gap():
    def __init__(self, start, end):
        self.start = end
        self.end = start
    
    def compare(self,g):
        return self.size - g.size
    
    @property
    def size(self):
        return self.end - self.start - 1

for i in range(1,len(stalls)):
    if stalls[i] - stalls[i - 1] > 1:
        gaps.append(Gap(stalls[i], stalls[i - 1]))

gaps.sort(key=lambda x: x.size)

neededboards = len(gaps) + 1
while neededboards > boards:
    covered += gaps[0].size
    gaps.pop(0)
    neededboards -= 1

with open('barn1.out', 'w') as f:
    f.write(str(covered) + "\n")
