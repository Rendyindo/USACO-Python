"""
ID: rendyar1
LANG: PYTHON3
TASK: beads
"""

def chainlength(s):
    count = 1
    lastColor = s[0]
    currColor = None
    switchedColor = False
    i = 1
    while i < len(s):
        currColor = s[i]
        if currColor == 'w':
            count += 1
        else:
            if lastColor == 'w' or lastColor == currColor:
                lastColor = currColor
                count += 1
            else:
                if switchedColor:
                    break
                else:
                    switchedColor = True
                    lastColor = currColor
                    count += 1
        i += 1
    return count

with open("beads.in", "r") as f:
    line = f.readlines()[1]

maxLength = 0
i = 0
while i < len(line):
    currLength = chainlength((line[i:len(line)] + line[0:i]).replace("\n", "").replace("\r", "").replace("\r\n", ""))
    if currLength > maxLength:
        maxLength = currLength
    i += 1

with open("beads.out", "w") as f:
    f.write(str(maxLength) + "\n")