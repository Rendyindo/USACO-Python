"""
ID: rendyar1
LANG: PYTHON3
TASK: dualpal
"""
def isPal(s):
    return s == s[::-1]

def toBase(i,b):
    s = ""
    chars = ['0','1','2','3','4','5','6','7','8','9',
             'A','B','C','D','E','F','G','H','I','J']
    while i >= b:
        s = chars[i%b] + s
        i = int(i/b)
    return chars[i] + s
with open("dualpal.in", "r") as f:
    n, s = list(map(int, f.readline().split(" ")))

cnt = 0
i = 1
nums = []
while cnt < n:
    while i <= s:
        i += 1
    basecnt = 0
    for b in range(2,11):
        if int(toBase(i,b)) > s:
            if isPal(toBase(i,b)):
                print("{} - {} - {}".format(i, b, toBase(i,b)))
                basecnt += 1
    if basecnt >= 2:
        nums.append(i)
        cnt += 1
    i += 1

with open("dualpal.out", "w") as f:
    nums.sort()
    nums = list(map(str, nums))
    f.write("\n".join(nums) + "\n")