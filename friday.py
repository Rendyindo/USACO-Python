"""
ID: 
LANG: PYTHON3
TASK: friday
"""
daylist = ["Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]

start = 0
days = {}
for v in daylist:
	days[v] = 0
year = 1900

with open("friday.in", "r") as f:
	maxyear = 1900 + int(f.read()) - 1

mdays = [31,28,31,30,31,30,31,31,30,31,30,31]
def leap():
	global start
	if mdays[1] == 28:
		mdays[1] += 1
	for day in mdays:
		theday = daylist[start]
		days[theday] += 1
		start += day % 4
		if start > 6:
			start -= 7

def nonleap():
	global start
	if mdays[1] == 29:
		mdays[1] -= 1
	for day in mdays:
		theday = daylist[start]
		days[theday] += 1
		start += day % 4
		if start > 6:
			start -= 7

	
while year <= maxyear:
	if year % 4 == 0 and year % 400 == 0:
		leap()
	elif year % 100 != 0:
		if year % 100 % 4 == 0:
			leap()
		else:
			nonleap()
	elif year % 4 == 0 and year % 400 != 0:
		nonleap()
	elif year % 4 != 0:
		nonleap()
	year += 1

with open("friday.out", "w") as f:
	f.write(" ".join([str(v) for k,v in days.items()]) + "\n")