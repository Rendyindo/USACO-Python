"""
ID: 
LANG: PYTHON3
TASK: gift1
"""
from math import floor
b = {}
with open("gift1.in", "r") as f:
	lines = f.readlines()
	lines = [x.replace("\n", "") for x in lines]
	jumlahpemain = int(lines[0])
	pemain = {}
	for main in lines[1:jumlahpemain+1]:
		pemain[main] = 0
	barispemberi = jumlahpemain + 1
	nilai, jumlah = map(int, lines[barispemberi+1].split(" "))
	while jumlah != -1:
		pemberi = lines[barispemberi]
		bariskebagian = barispemberi + 1 + jumlah + 1
		orangkebagian = lines[barispemberi + 2:bariskebagian]
		if jumlah != 0:
			sisa = nilai % jumlah
			kasih = floor(nilai / jumlah)
			for orang in orangkebagian:
				pemain[orang] += kasih
			pemain[pemberi] -= nilai - sisa
		barispemberi = bariskebagian
		try:
			nilai, jumlah = map(int, lines[barispemberi+1].split(" "))
		except:
			jumlah = -1
with open("gift1.out", "w") as f:
	for orang in list(pemain.keys()):
		f.write("{} {}\n".format(orang.replace("\n", ""), pemain[orang]))
