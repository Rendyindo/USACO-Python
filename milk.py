"""
ID: rendyar1
LANG: PYTHON3
TASK: milk
"""
with open("milk.in", "r") as f:
    lines = f.readlines()
    need = int(lines[0].split(' ')[0])
    farmers = []
    for line in lines[1:]:
        farmers.append(list(map(int, line.replace("\n", "").split(" "))))
    farmers.sort()

i = 0
munz = 0
while need != 0:
    bottle = int(farmers[i][1])
    cost = int(farmers[i][0])
    if bottle > need:
        munz += cost * need
        need = 0
    else:
        munz += cost * bottle
        need -= bottle
    if need > 0:
        print("----------------")
        print("Bottle: {} - Cost: {} - Need: {}".format(bottle,cost,need))
        print("Munz: {}".format(munz))
    i += 1
with open("milk.out", "w") as f:
    f.write(str(munz) + "\n")