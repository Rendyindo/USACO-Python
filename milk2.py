"""
ID: rendyar1
LANG: PYTHON3
TASK: milk2
"""
with open("milk2.in", "r") as f:
    lines = f.readlines()[1:]
newlines = []
for l in lines:
    newlines.append(list(map(int,l.replace("\n", "").split(" "))))
i = 0
newlines.sort()
print(newlines)
start = newlines[i][0]
end = 0
cmilk = []
while i < len(newlines):
    if newlines[i][1] > end:
        end = newlines[i][1]
    if i+1 >= len(newlines):
        break
    nextstart = newlines[i+1][0]
    while nextstart < end:
        i += 1
        if newlines[i][1] > end:
            end = newlines[i][1]
        if i+1 >= len(newlines):
            break
        nextstart = newlines[i+1][0]
    if nextstart > end:
        cmilk.append(end-start)
        start = nextstart
    i += 1
if cmilk:
    cmilk.sort()
    conmilk = cmilk[-1]
else:
    conmilk = end - start

maxs = 0
i = 0
end2 = 0
while i < len(newlines):
    if i+1 >= len(newlines):
        break
    nextstart = newlines[i+1][0]
    if newlines[i][1] > end2:
        end2 = newlines[i][1]
    if nextstart > end2:
        if nextstart - end2  > maxs:
            maxs = nextstart - end2
    i += 1

with open("milk2.out", "w") as f:
    f.write("{} {}\n".format(conmilk, maxs))