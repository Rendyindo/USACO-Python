"""
ID: rendyar1
LANG: PYTHON3
TASK: namenum
"""
num = {
    2: ['A','B','C'],
    3: ['D','E','F'],
    4: ['G','H','I'],
    5: ['J','K','L'],
    6: ['M','N','O'],
    7: ['P','R','S'],
    8: ['T','U','V'],
    9: ['W','X','Y']
}
def rln(s):
    return s.replace("\n", "")
with open("namenum.in", "r") as f:
    numlist = f.readline().replace("\n", "")
with open("dict.txt", 'r')  as f:
    dicc = list(map(rln, f.readlines()))

res = []

def mapstr(s):
    r = []
    for char in list(s):
        for key, val in num.items():
            if char in val:
                r.append(key)
    return "".join(list(map(str, r)))

for s in dicc:
    v = mapstr(s)
    if v == numlist:
        res.append(s)

with open("namenum.out", "w") as f:
    if res:
        f.write("\n".join(res) + "\n")
    if not res:
        f.write("NONE\n")