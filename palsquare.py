"""
ID: rendyar1
LANG: PYTHON3
TASK: palsquare
"""
def isPal(s):
    return s == s[::-1]
def toBase(i,b):
    s = ""
    chars = ['0','1','2','3','4','5','6','7','8','9',
             'A','B','C','D','E','F','G','H','I','J']
    while i >= b:
        s = chars[i%b] + s
        i = int(i/b)
    return chars[i] + s
with open("palsquare.in", "r") as f:
    base = int(f.readline().replace("\n", ""))
with open("palsquare.out", "w") as f:
    for i in range(1, 301):
        sqr = toBase(i * i, base)
        if isPal(sqr):
            f.write(toBase(i, base) + " " + sqr + "\n")
