"""
ID: rendyar1
LANG: PYTHON3
TASK: transform
"""
import sys
def removenl(s):
    return s.replace("\n", "")

with open("transform.in", "r") as f:
    lines = f.readlines()
    size = int(lines[0])
    origin = list(map(removenl, (lines[1:size + 1])))
    result = list(map(removenl, lines[size + 1:size * 2 + 2]))



class sqr():
    def __init__(self, l):
        self.l = l
        self.size = len(l)

    def rotate(self):
        res = []
        for n in range(0,self.size):
            res.append([])
            for n2 in range(0, self.size):
                res[n].append([])
        y = 0
        x = 0
        for line in self.l:
            if y == self.size:
                y = 0
            charlist = list(line)
            for char in charlist:
                if x == self.size:
                    x = 0
                res[x][len(self.l[0]) - 1 - y] = self.l[y][x]
                x += 1
            y += 1
        result = []
        for ni in range(0,len(res)):
            result.append("".join(res[ni]))
        self.l = result

    def reflect(self):
        res = []
        for n in range(0,self.size):
            res.append([])
            for n2 in range(0, self.size):
                res[n].append([])
        y = 0
        x = 0
        for line in self.l:
            if y == self.size:
                y = 0
            charlist = list(line)
            for char in charlist:
                if x == self.size:
                    x = 0
                res[x][len(self.l[0]) - 1 - y] = self.l[x][y]
                x += 1
            y += 1
        result = []
        for ni in range(0,len(res)):
            result.append("".join(res[ni]))
        self.l = result
        
    @property
    def obj(self):
        return self.l

def cw(num,f):
    if sqr.obj == result:
        f.write(str(num) + "\n")
        sys.exit()
    if num == 7:
        f.write(str(num) + "\n")
        
with open("transform.out", "w") as f:
    sqr = sqr(origin)
    sqr.rotate()
    cw(1, f)
    sqr.rotate()
    cw(2,f)
    sqr.rotate()
    cw(3,f)
    sqr.rotate()
    sqr.reflect()
    cw(4,f)
    sqr.rotate()
    cw(5,f)
    sqr.rotate()
    cw(5,f)
    sqr.rotate()
    cw(5,f)
    sqr.rotate()
    cw(6,f)
    cw(7,f)